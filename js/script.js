$(document).ready(function(){
	
	// about section height	
	var doc_width = $(document).width();
	
	if (doc_width <= 550) {
		var nav_pic_height = Math.round($('.leader_box .leader_text').width());
	} else {
		var nav_pic_height = Math.round($('.leader_box .leader_text').width() * 0.47895501);
	}
	if (doc_width <= 1336) {
		var max_bio_height = Math.max($('.leader_box .leader_text').height());
		var bio_height = max_bio_height + nav_pic_height;
		var bio_block_height = max_bio_height + nav_pic_height + 52;
	} else {
		var max_bio_height = Math.max($('.leader_box .leader_text').width(),$('.leader_box .leader_text').height());
		var bio_block_height = max_bio_height;
	}
	
	$('.leaders_expanded').height(bio_block_height);
	
	// solutions equal section heights	
	$(function(){
		var max_column_height = Math.max($('.consulting_slide .single_column').height(),$('.implementation_slide .single_column').height(),
		$('.execution_slide .single_column').height(),$('.analytics_slide .single_column').height());
		$('#solutions .solutions_txt_wrap').height(max_column_height);
	});
	
	// footer equal section heights
	$(function(){
		var max_footer_height = $('.left_column.twitter_feed').height();
		$('.contact_form textarea').height(max_footer_height - 157);
		$('.footer_social_btns').css('margin-top', (max_footer_height - 118));
	});
	

	
	
	$('.implementation_slide').css('opacity', ('0.0')).css('margin-left', ('-1000%'));
	$('.execution_slide').css('opacity', ('0.0')).css('margin-left', ('-1000%'));
	$('.analytics_slide').css('opacity', ('0.0')).css('margin-left', ('-1000%'));
	
	$('.together_slide').css('opacity', ('0.0')).css('margin-left', ('-1000%'));
	$('.leadership_slide').css('opacity', ('0.0')).css('margin-left', ('-1000%'));
	
	$('.consulting_quote').hide();
	$('.implementation_quote').hide();
	$('.execution_quote').hide();
	$('.analytics_quote').hide();	
	
	$('.leaders_expanded').hide();
	$('.leader_box').css('opacity', ('0.0'));
	

	
	
	
	// start timed home slideshow
    $(function(){
	  setTimeout(function(){
		autoSlideshow();
	  }, 2000)
    });
	
	
  
	// bug orientation portrait/lanscape IOS //
	if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
	var viewportmeta = document.querySelector('meta[name="viewport"]');
	if (viewportmeta) {
		viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
		document.addEventListener('orientationchange', function () {
			viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1';
		}, false);
	  }
	}
	
	
	
	// main nav
	
	var win_width = $(window).width();
	
	$('a.home').click(function(e) {
		e.preventDefault();
		// scroll document
		$.scrollTo(0,'slow');
   });
	$('a.solutions').click(function(e) {
		e.preventDefault();
		// scroll document / offset for fixed nav
		if (win_width > 728) {
			$.scrollTo('#solutions_wrap','slow', {
				offset:-78
			});
		} else {
			$.scrollTo('#solutions_wrap','slow', {});
		}
   });
	$('a.clients').click(function(e) {
		e.preventDefault();
		// scroll document / offset for fixed nav
		if (win_width > 728) {
			$.scrollTo('#clients_wrap','slow', {
				offset:-78
			});
		} else {
			$.scrollTo('#clients_wrap','slow', {});
		}
	});
	$('a.about').click(function(e) {
		e.preventDefault();
		// scroll document / offset for fixed nav
		if (win_width > 728) {
			$.scrollTo('#about_wrap','slow', {
				offset:-78
			});
		} else {
			$.scrollTo('#about_wrap','slow', {});
		}
	});
	$('a.contact').click(function(e) {
		e.preventDefault();
		// scroll document / offset for fixed nav
		if (win_width > 728) {
			$.scrollTo('#footer_wrap','slow', {
				offset:-78
			});
		} else {
			$.scrollTo('#footer_wrap','slow', {});
		}
	});
	$('.rounded_arrow a').click(function(e) {
		e.preventDefault();
		// scroll document / offset for fixed nav
		if (win_width > 728) {
			$.scrollTo('#solutions_wrap','slow', {
				offset:-78
			});
		} else {
			$.scrollTo('#solutions_wrap','slow', {});
		}
   });
	
	
	
	// solution section
   $('#consulting a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) {
		} else {
			// fadein section
			$('.open').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('open');
			$('.selected').removeClass('selected');
			$(this).addClass('selected');
			$('.consulting_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('open');
		}
   });
   $('#implementation a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) {
		} else {
			// fadein section
			$('.open').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('open');
			$('.selected').removeClass('selected');
			$(this).addClass('selected');
			$('.implementation_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('open');
		}
   });
   $('#execution a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) {
		} else {
			// fadein section
			$('.open').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('open');
			$('.selected').removeClass('selected');
			$(this).addClass('selected');
			$('.execution_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('open');
		}
   });
   $('#analytics a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('selected')) {
		} else {
			// fadein section
			$('.open').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('open');
			$('.open').fadeOut().removeClass('open');
			$('.selected').removeClass('selected');
			$(this).addClass('selected');
			$('.analytics_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('open');
		}
   });
   
   
   
   // about section
   $('#history a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('stay')) {
		} else {
			// fadein section
			$('.view').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('view');
			$('.stay').removeClass('stay');
			$(this).addClass('stay');
			$('.history_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('view');
		}
   });
   $('#together a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('stay')) {
		} else {
			// fadein section
			$('.view').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('view');
			$('.stay').removeClass('stay');
			$(this).addClass('stay');
			$('.together_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('view');
		}
   });
   $('#leadership a').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('stay')) {
		} else {
			// fadein section
			$('.view').animate({opacity: '0.0'}).css('visibility', ('hidden')).css('margin-left', ('-1000%')).removeClass('view');
			$('.stay').removeClass('stay');
			$(this).addClass('stay');
			$('.leadership_slide').css('margin-left', ('0')).css('visibility', ('visible')).delay(400).animate({opacity: '1.0'}).addClass('view');
		}
   });
   
   
   
   // leadership section expand
    $('.leader_pic').click(function(e) {
		e.preventDefault();
		// fadein section
		if ($('.leaders_expanded').hasClass('leader_open')) {
		} else {
			$('.leaders_expanded').slideDown().addClass('leader_open');
		}
		// margin for bio position
   });
   $('.aaron_btn').click(function(e) {
		e.preventDefault();
		if ($('.arron_bio').hasClass('current_leader')) {
		} else {
			// fadeOut section
			$('.current_leader').animate({opacity: '0.0'}).css('margin-left', ('0')).removeClass('current_leader');
			// fadein section
			var currentBio = 1;
			var bioMargin = (((currentBio - 1) * -5) + '%');
			$('.arron_bio').css('margin-left', (bioMargin)).delay(400).animate({opacity: '1.0'}).addClass('current_leader');
		}
   });
   $('.chris_btn').click(function(e) {
		e.preventDefault();
		if ($('.chris_bio').hasClass('current_leader')) {
		} else {
			// fadeOut section
			$('.current_leader').animate({opacity: '0.0'}).css('display', ('0')).removeClass('current_leader');
			// fadein section
			var currentBio = 2;
			var bioMargin = (((currentBio - 1) * -5) + '%');
			$('.chris_bio').css('margin-left', (bioMargin)).delay(400).animate({opacity: '1.0'}).addClass('current_leader');
		}
   });
   $('.carol_btn').click(function(e) {
		e.preventDefault();
		if ($('.carol_bio').hasClass('current_leader')) {
		} else {
			// fadeOut section
			$('.current_leader').animate({opacity: '0.0'}).css('margin-left', ('0')).removeClass('current_leader');
			// fadein section
			var currentBio = 3;
			var bioMargin = (((currentBio - 1) * -5) + '%');
			$('.carol_bio').css('margin-left', (bioMargin)).delay(400).animate({opacity: '1.0'}).addClass('current_leader');
		}
   });
   $('.close_btn').click(function(e) {
		e.preventDefault();
		// fadeOut section
		$('.current_leader').animate({opacity: '0.0'}).css('margin-left', (0)).removeClass('current_leader');
		$('.leaders_expanded').delay(400).slideUp().removeClass('leader_open');

   });
   
   
   
   // rounded arrow fade away
   $(window).scroll(function() {
  		// get the height of #wrap
 		var y = $(window).scrollTop();
		var position = ((y - 200) * -0.1);
		if (y > 200) {
			$('.rounded_arrow img').css({
				top: (position)
  			});
		} else {
			$('.rounded_arrow img').css({
				top: (0)
  			});
		}
   });
   
   
   
   
   // contact form
   $(".nameInput").val("Name");
        $(".nameInput").addClass("holderText");

        $(".nameInput").focus(function() {
            if ($(".nameInput").val() == "Name") {
                $(".nameInput").removeClass("holderText");
                $(".nameInput").val("");
            }
        });

        $(".nameInput").blur(function() {
            if ($(".nameInput").val() == "") {
                $(".nameInput").addClass("holderText");
                $(".nameInput").val("Name");
            }
        });
		
	$(".emailInput").val("Email");
        $(".emailInput").addClass("holderText");

        $(".emailInput").focus(function() {
            if ($(".emailInput").val() == "Email") {
                $(".emailInput").removeClass("holderText");
                $(".emailInput").val("");
            }
        });

        $(".emailInput").blur(function() {
            if ($(".emailInput").val() == "") {
                $(".emailInput").addClass("holderText");
                $(".emailInput").val("Email");
            }
        });
		
		$(".messageText").val("Message");
        $(".messageText").addClass("holderText");

        $(".messageText").focus(function() {
            if ($(".messageText").val() == "Message") {
                $(".messageText").removeClass("holderText");
                $(".messageText").val("");
            }
        });

        $(".messageText").blur(function() {
            if ($(".messageText").val() == "") {
                $(".messageText").addClass("holderText");
                $(".messageText").val("Message");
            }
        });
		
		// click submit button
	    $('.submit_btn').click(function(e) {
			e.preventDefault();
			var name = $(".nameInput").val();  
			var email = $(".emailInput").val();
			var message = $(".messageText").val();
			var dataString = 'name='+ name + '&email=' + email + '&message=' + message;  
			$.ajax({  
			  type: "post",  
			  url: "FormtoEmail.php",  
			  data: dataString,  
			  success: function() {  
				$('.thankyou_message').css('display', 'block');    
			  }  
			});  
			return false;
	    });  
   
   
   
  
});





// home quotes
function autoSlideshow() {
   
   if ($('.consulting_quote').hasClass('current_quote')) {
	   $('.consulting_quote').fadeIn();
	   $('.consulting_quote').delay(3000).fadeOut().removeClass('current_quote');
	   $('.implementation_quote').delay(3900).addClass('current_quote');
   } else if ($('.implementation_quote').hasClass('current_quote')) {  
   	   $('.implementation_quote').fadeIn();
	   $('.implementation_quote').delay(3000).fadeOut().removeClass('current_quote');
	   $('.execution_quote').delay(3900).addClass('current_quote'); 
    } else if ($('.execution_quote').hasClass('current_quote')) {  
   	   $('.execution_quote').fadeIn();
	   $('.execution_quote').delay(3000).fadeOut().removeClass('current_quote');
	   $('.analytics_quote').delay(390).addClass('current_quote'); 
    } else if ($('.analytics_quote').hasClass('current_quote')) {  
   	   $('.analytics_quote').fadeIn();
	   $('.analytics_quote').delay(3000).fadeOut().removeClass('current_quote');
	   $('.consulting_quote').delay(3900).addClass('current_quote'); 
   }
   
	setTimeout(autoSlideshow, 4000);

}






// change equal height when browser is resized
$(window).resize(function() {
	
	// about equal section heights
	var doc_width = $(document).width();
	
	if (doc_width <= 550) {
		var nav_pic_height = Math.round($('.leader_box .leader_text').width());
	} else {
		var nav_pic_height = Math.round($('.leader_box .leader_text').width() * 0.47895501);
	}
	if (doc_width <= 1336) {
		var max_bio_height = Math.max($('.leader_box .leader_text').height());
		var bio_height = max_bio_height + nav_pic_height;
		var bio_block_height = max_bio_height + nav_pic_height + 52;
	} else {
		var max_bio_height = Math.max($('.leader_box .leader_text').width(),$('.leader_box .leader_text').height());
		var bio_block_height = max_bio_height;
	}
	
	$('.leaders_expanded').height(bio_block_height);
	
	// solutions equal section heights	
	$(function(){
		var max_column_height = Math.max($('.consulting_slide .single_column').height(),$('.implementation_slide .single_column').height(),
		$('.execution_slide .single_column').height(),$('.analytics_slide .single_column').height());
		$('#solutions .solutions_txt_wrap').height(max_column_height);
	});
	
	// footer equal section heights
	$(function(){
		var max_footer_height = $('.left_column.twitter_feed').height();
		$('.contact_form textarea').height(max_footer_height - 157);
		$('.footer_social_btns').css('margin-top', (max_footer_height - 118));
	});
	
	
});