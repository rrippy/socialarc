<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<meta name="keywords" content="socialarc, social media, strategy, app, brand development">
<link rel="shortcut icon" href="http://www.socialarc.com/favicon.ico" type="image/x-icon" /> 

<title>socialarc | social marketing</title>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,400italic,600italic,700,700italic|Lustria' rel='stylesheet' type='text/css'>

<link href="socialarc.css" rel="stylesheet" />

<script src="js/modernizr-1.7.min.js"></script>


 <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<div id="wrapper">


<!--HEADER-->
<div id="header_wrap">
    <header>
    	<h1><a href="#" class="home"><img src="images/socialarc_logo.png" alt="socialarc" title="socialarc" /></a>
        <span>socialarc - social marketing</span></h1>
        
        <nav class="right_column">
        	<ul>
            	<li class="wider"><a href="#" class="solutions">Solutions</a></li>
                <li class="wider"><a href="#" class="clients">Clients</a></li>
                <li><a href="#" class="about">About</a></li>
                <li><a href="http://www.socialarc.com/socialarc-blog/" class="blog">Blog</a></li>
                <li><a href="#" class="contact">Contact</a></li>
            </ul>
        </nav>
    
    </header>
</div><!--#header_wrap-->



<div class="social_links">
	<a href="http://www.socialarc.com/blog/index.php" target="_blank" class="rss"></a>
    <a href="http://www.facebook.com/socialarc" target="_blank" class="facebook"></a>
    <a href="https://twitter.com/socialarc" target="_blank" class="twitter"></a>
    <a href="http://www.linkedin.com/company/socialarc" target="_blank" class="linkedin"></a>
    <a href="https://plus.google.com/107636132629021549319/posts" target="_blank" class="googleplus"></a>
</div>



<!--HOME SECTION-->
<div id="home_wrap">
    <section id="home">
    	<div class="home_graphic">
        	<img class="infographic" src="images/home_graphic.png" />
            <img class="infographic_all" src="images/home_graphic_info.png" />
            
			<div class="infographic_block consulting">
            	<img src="images/icon_consulting_blue.png" class="info_icon" />
                <h5>Consulting</h5>
                Social Strategy and Organizational Development
            </div>
            
            <div class="infographic_block implementation">
            	<img src="images/icon_implementation_blue.png" class="info_icon" />
                <h5>Implementation</h5>
                Social Content Development
            </div>
            
            <div class="infographic_block execution">
            	<img src="images/icon_execution_blue.png" class="info_icon" />
                <h5>Execution</h5>
                Owned and Earned Social Management
            </div>
            
             <div class="infographic_block analytics">
            	<img src="images/icon_analytics_blue.png" class="info_icon" />
                <h5>Insights and Analytics</h5>
                Success Measurement
            </div>
            
            <div class="infographic_block socialarc">
            	<img src="images/socialarc_logo.png" alt="socialarc" title="socialarc" />
            </div>
            
        </div>
        
        
        <div class=" home_quote consulting_quote current_quote">Grounded in insights, data and a clear understanding of your business objectives</div>
        <div class=" home_quote implementation_quote">Taking your social strategy from concept to realization</div>
        <div class=" home_quote execution_quote">Supporting your paid, owned, and earned social programs</div>
        <div class=" home_quote analytics_quote">Giving you the data to make informed decisions</div>

    </section>
</div><!--#home_wrap-->    


<div class="quote_wrap">
    <section class="block_quote">
    	<h4>Our social thinking is based on a strategic approach to thoroughly understanding social conversations, being able to provide clients with 
        a clear social roadmap with actionable recommendations and executing to achieve set business goals.</h4>
    </section>
</div><!--.quote_wrap-->



<!--SOLUTIONS SECTION-->
<div id="solutions_wrap">
	
    <div class="rounded_arrow"><a href="#"><img src="images/rounded_arrow.png" /></a></div>
    
    <section id="solutions">
    
    	<div class="left_column">
        	<hgroup>
         		<h2>Solutions</h2>
            	<p>The biggest challenge that businesses face in “social” is finding the right solution to help them understand the constantly evolving social landscape.  
                As an integrated social business partner, we help clients understand their audience and gain the scale needed to make an impact on business objectives.</p>
           </hgroup>
           
           <!-- solutions menu -->
           <ul>
           		<li id="consulting"><a href="#" class="selected">
                	<div class="btn_icon consulting_icon"></div>
                    <h3>Consulting</h3>
                </a></li>
                
                <li id="implementation"><a href="#">
                	<div class="btn_icon implementation_icon"></div>
                    <h3>Implementation</h3>
                </a></li>
                
                <li id="execution"><a href="#">
                	<div class="btn_icon execution_icon"></div>
                    <h3>Execution</h3>
                </a></li>
                
                <li id="analytics"><a href="#">
                	<div class="btn_icon analytics_icon"></div>
                    <h3>Insights and Analytics</h3>
                </a></li>
           </ul>
           
        </div><!-- .left_column -->
        
        <!-- solutions slideshow -->
        <div class="right_column consulting_slide open">
        	<img src="images/solutions_1.jpg" />
        
        	<div class="solutions_txt_wrap">
                <div class="single_column">
                <p>Our consulting service focuses on helping our clients’ navigate through their most critical issues and opportunities in social across various industries and geographies. 
                We work with some of the world’s leading brands and through our deep, functional expertise in strategy, social business organizational development, owned and earned channel 
                planning, measurement, KPI’s, and benchmarking architecture we help them capture value through optimizing the sum of parts, not just individual pieces.</p>
                </div>
           </div>
            
        </div><!-- .right_column -->
        
        <div class="right_column implementation_slide">
        	<img src="images/solutions_2.jpg" />
        
        	<div class="solutions_txt_wrap">
                <div class="single_column">
                <p>Once your social strategy has been identified, we help bring it to life. Our breadth of services includes in-house creative, application development, social playbooks, best 
                practices and training, content planning and development, social channel integration and paid media support planning. Our team knows how to bring a sound social strategy to 
                life, in your brand voice and in a way that truly engages your audience.</p>
                </div>
           </div>
            
        </div><!-- .right_column -->
        
        <div class="right_column execution_slide">
        	<img src="images/solutions_3.jpg" />
        	
            <div class="solutions_txt_wrap">
                <div class="single_column">
                <p>Socialarc's execution services drive the organic and viral reach of your content by working across owned and earned social channels to reach your audience. Our Engagement Teams 
                bring together content, creative and analytics to amplify the union between your brand and your audience, and measure success to business objectives.</p>
                </div>
           </div>
            
        </div><!-- .right_column -->
        
        <div class="right_column analytics_slide">
        	<img src="images/solutions_4.jpg" />
        	
            <div class="solutions_txt_wrap">
                <div class="single_column">
                <p>When you consider content creation, media buys, app development and promotions, your investments can add up pretty fast. By having all the analytics available to you, 
                from content performance to who your audience is, you can make more informed decisions about your content strategy, while staying in-tune with the constantly changing 
                landscape of social media. By making data-driven decisions about that spend, in the end you will be more effective in building your brand voice, engaging with your audience 
                and optimizing your spend.</p>
                </div>
            </div>
            
        </div><!-- .right_column -->
        
        <div class="clear"></div>
       
    </section>
</div><!--#solutions_wrap-->



<!--CLIENTS SECTION-->
<div id="clients_wrap">
    <section id="clients">
    
    	<div class="left_column">
        	<hgroup>
         		<h2>Clients</h2>
            	<p>Socialarc has designed and executed highly successful social campaigns for a wide range of leading brands and industries. 
                From Fortune 500 companies to venture-backed startups and non-profits, our experience and success on client engagements gives us a clear view of what works in social.</p>
           </hgroup>
       </div><!-- .left_column -->
        

        <div class="right_column">
        
        	<div class="client_logo"><img src="images/Visa.jpg" /></div>
            <div class="client_logo"><img src="images/San-Disk.jpg" /></div>
            <div class="client_logo"><img src="images/Verizon.jpg" /></div>
            <div class="client_logo"><img src="images/Lenovo.jpg" /></div>
            <div class="client_logo"><img src="images/Capital-One.jpg" /></div>
            <div class="client_logo"><img src="images/photobuket.jpg" /></div>
            <div class="client_logo"><img src="images/philips.jpg" /></div>
            <div class="client_logo"><img src="images/samsung.jpg" /></div>
            <div class="client_logo"><img src="images/fair-trade.jpg" /></div>
            <div class="client_logo"><img src="images/michelob-ultra.jpg" /></div>
            <div class="client_logo"><img src="images/edu.jpg" /></div>
            <div class="client_logo"><img src="images/United-healthcare.jpg" /></div>     
            
        </div><!-- .right_column -->
        
        <div class="clear"></div>
    
    </section>
</div><!--#clients_wrap-->



<!--ABOUT SECTION-->
<div id="about_wrap">
    <section id="about">
    
    	<div class="left_column">
        	<hgroup>
         		<h2>About</h2>
            	<p>Socialarc is a strategic business partner that focuses on helping both leading and emerging brands develop as a social business and connect meaningfully with their audiences.</p>
           </hgroup>
           
           <!-- about menu -->
           <ul>
           		<li id="history"><a href="#" class="stay">
                	<div class="btn_icon history_icon"></div>
                    <h3>History</h3>
                </a></li>
                
                <li id="together"><a href="#">
                	<div class="btn_icon together_icon"></div>
                    <h3>Working Together</h3>
                </a></li>
                
                <li id="leadership"><a href="#">
                	<div class="btn_icon leadership_icon"></div>
                    <h3>Leadership</h3>
                </a></li>
                
           </ul>
           
        </div><!-- .left_column -->
         
        
        <!-- about slideshow -->
        <div class="right_column history_slide view">
        
            <div class="single_column">
            <p>In 2006, Facebook reaches 12m users and the first tweet is sent. With that, Aaron Mann launches a social media monitoring startup focused on hearing what 
            consumers are saying about brands and products. As social media continues to grow in popularity, clients begin to ask "how can I meet my customers in social?"</p> 

			<p>Launched in 2009 to help answer that question, the Socialarc team is comprised of strategists, creatives, analysts, technologists, and brand engagement experts 
            who thrive on working at the nexus of content, creative and analytics.</p>

            </div>
            
        </div><!-- .right_column -->
        
        <div class="right_column together_slide">
        
            <div class="single_column">
            <p>Just like our approach to helping clients connect with their fans in social, we take the same philosophy in cultivating a relationship with our clients through 
            clear roles and processes.</p>

			<p>We don’t believe in layers, instead you’ll work directly with the people actually doing the work. Your project manager streamlines communications, ensures completed deliverables, 
            manages budgets and will be accountable for reporting everything we do; evaluating all of our efforts against key KPIs and business objectives. Having a strong relationship will 
            produce the best work and lead to successful business results. We care about our client’s business objectives like it’s our own and we’re in it together.</p>

            </div>
            
        </div><!-- .right_column -->
        
        

         <div class="right_column leadership_slide">
         
            <!-- Leadership expanded -->
            <div class="leaders_expanded">
            
            	<div class="leader_box arron_bio">
                    <div class="large_pic"><img src="images/Aaron.jpg" /></div>
                    <div class="leader_text">
                        <div class="close_btn"><a href="#"></a></div>
                        <div class="leader_name"><h3>Aaron Mann</h3></div>
                        <div class="leader_title">CEO, Chief Strategist</div>
                        <a href="mailto:aaron.mann@socialarc.com" class="leader_email">aaron.mann@socialarc.com</a>
                        <p>Aaron leads client strategy and loves the challenge of bringing the rapidly evolving social world to life within an overall brand strategy. 
                        His work with clients and partners is a constant learning experience that keeps him (and the team) on the leading edge of social business development. 
                        Aaron’s career has been focused on social media since 2006 when he founded RelevantMind, one of the first social media curated content platforms.</p>
                        <p>On a personal level, Aaron enjoys snowboarding with his daughter and is an avid fan of anything two-wheeled, from mountain bikes to motorcycles.</p>
                    </div>
                </div>
                
            	<div class="leader_box chris_bio">
                    <div class="large_pic"><img src="images/Chris.jpg" /></div>
                    <div class="leader_text">
                        <div class="close_btn"><a href="#"></a></div>
                        <div class="leader_name"><h3>Chris Adorna</h3></div>
                        <div class="leader_title">VP, Creative</div>
                        <a href="mailto:christopher.adorna@socialarc.com" class="leader_email">christopher.adorna@socialarc.com</a>
                        <p>Chris has been doing digital since 1998 and has held senior creative and strategy roles at startups like Yellowpages.com and Context Optional, as well as 
                        at agencies like WONGDOODY, Direct Partners, and RAPP. He has worked with a long list of clients including T-Mobile, Sony, AT&T, Bank of America, Schwab, Neutrogena, 
                        Toyota, Lexus, Evite, Tapjoy, Visa, and Kodak Gallery. Chris brings together his expertise in design, data, technology, and marketing to lead creative and product 
                        development at Socialarc.</p>
                    </div>
             	</div>
                
                <div class="leader_box carol_bio">
                    <div class="large_pic"><img src="images/Carol.jpg" /></div>
                    <div class="leader_text">
                        <div class="close_btn"><a href="#"></a></div>
                        <div class="leader_name"><h3>Carol Rooney</h3></div>
                        <div class="leader_title">VP, Client Services</div>
                        <a href="mailto:carol.rooney@socialarc.com" class="leader_email">carol.rooney@socialarc.com</a>
                        <p>Carol joined Socialarc in 2009. She draws from her extensive experience working for technology firms such as Ascend, Lucent, Zhone, and Riverbed for her current 
                        role at Socialarc. As Vice President of Client Services, she oversees both our Engagement and Community Management teams, working closely both with clients and 
                        internal teams to keep campaigns running smoothly and successfully. She began her career in Human Relations management, developing staffing solutions for clients 
                        such as Gap, Lucent, and Oracle. Her social media career began with clients such as National Geographic Kids and PBS Kids.</p>
                    </div>
             	</div>
            
            	<div class="clear"></div>
                
            </div><!-- .leaders_expanded -->
            
            <!-- Leadership thumbnails -->
            <div class="leaders_thumbs">
            
                <div class="leader_btn">
                    <a href="#" class="leader_pic aaron_btn"><img src="images/Aaron_thumb.jpg" /><div class="expand_btn"></div></a>
                    <div class="leader_info">
                        <div class="leader_name">Aaron Mann</div>
                        <div class="leader_title">CEO, Chief Strategist</div>
                        <a href="mailto:aaron.mann@socialarc.com" class="leader_email">aaron.mann@socialarc.com</a>
                    </div>
                </div>
                
                <div class="leader_btn">
                    <a href="#" class="leader_pic chris_btn"><img src="images/Chris_thumb.jpg" /><div class="expand_btn"></div></a>
                    <div class="leader_info">
                        <div class="leader_name">Chris Adorna</div>
                        <div class="leader_title">VP, Creative</div>
                        <a href="mailto:christopher.adorna@socialarc.com" class="leader_email">christopher.adorna@socialarc.com</a>
                    </div>
                </div>
                
                <div class="leader_btn">
                    <a href="#" class="leader_pic carol_btn"><img src="images/Carol_thumb.jpg" /><div class="expand_btn"></div></a>
                    <div class="leader_info">
                        <div class="leader_name">Carol Rooney</div>
                        <div class="leader_title">VP, Client Services</div>
                        <a href="mailto:carol.rooney@socialarc.com" class="leader_email">carol.rooney@socialarc.com</a>
                    </div>
                </div>
                
            </div><!-- .leaders_thumbs -->
            
        </div><!-- .right_column -->
        
        <div class="clear"></div>
    
    </section>
</div><!--#about_wrap-->



<!--FOOTER-->
<div id="footer_wrap">
    <footer>
    
    	<div class="left_column">
        	<hgroup>
         		<h2>Contact Us</h2>
           </hgroup>  
        </div><!-- .left_column -->
        
        <div class="left_column contact_form">  
        	
            <form action="FormtoEmail.php" method="post" name="contactForm">
                <input id="name" value="Name" class="holderText nameInput" maxlength="80" name="name" type="text" />
                <input id="email" value="Email" class="holderText emailInput" maxlength="80" name="email" type="text" />
                <textarea value="Message" class="holderText messageText" rows="10" name="message"></textarea>
                <input type="submit" name="Submit" value="Submit" class="submit_btn">
            </form>
            
            <div class="thankyou_message">Thank you for contacting Socialarc. We will be in touch with you shortly.</div>
        
        </div><!-- .left_column.contact_form -->
        
        
        
        <div class="left_column contact_info"> 
        	<div class="contact_main_info"> 
            	<p>6550 Vallejo St. Suite 100<br />
				Emeryville, CA 94608</p>
				<p><a href="mailto:hello@socialarc.com">hello@socialarc.com</a><br />
				510.868.2787</p>
                <div class="footer_social_btns">
					<a href="http://www.socialarc.com/blog/index.php" target="_blank" class="rss"></a>
    				<a href="http://www.facebook.com/socialarc" target="_blank" class="facebook"></a>
    				<a href="https://twitter.com/socialarc" target="_blank" class="twitter"></a>
    				<a href="http://www.linkedin.com/company/socialarc" target="_blank" class="linkedin"></a>
    				<a href="https://plus.google.com/107636132629021549319/posts" target="_blank" class="googleplus"></a>
				</div>
            </div>
        </div><!-- .left_column.contact_info -->
        
        
        <div class="left_column twitter_feed"> 
        	<div class="twitter_bar">
            	<a href="https://twitter.com/socialarc" target="_blank">@socialarc</a>
            </div>
            <div class="twitter_posts">
            	<div class="twitter_post">
                	Socialarc Back up your #brand’s #Facebook content in 5 easy steps: <a href="ow.ly/hlcDZ" target="_blank">ow.ly/hlcDZ</a>
                    <div class="twitter_meta">
                    	8 hours ago &#8226; <a href="#" target="_blank">reply</a> &#8226; <a href="#" target="_blank">retweet</a> &#8226; <a href="#" target="_blank">favorite</a>
                    </div>
                </div>
                <div class="twitter_post">
                	Socialarc Back up your #brand’s #Facebook content in 5 easy steps: <a href="ow.ly/hlcDZ" target="_blank">ow.ly/hlcDZ</a>
                    <div class="twitter_meta">
                    	10 hours ago &#8226; <a href="#" target="_blank">reply</a> &#8226; <a href="#" target="_blank">retweet</a> &#8226; <a href="#" target="_blank">favorite</a>
                    </div>
                </div>
                <div class="twitter_post">
                	Socialarc Back up your #brand’s #Facebook content in 5 easy steps: <a href="ow.ly/hlcDZ" target="_blank">ow.ly/hlcDZ</a>
                    <div class="twitter_meta">
                    	12 hours ago &#8226; <a href="#" target="_blank">reply</a> &#8226; <a href="#" target="_blank">retweet</a> &#8226; <a href="#" target="_blank">favorite</a>
                    </div>
                </div>
            </div> 
        </div><!-- .left_column.twitter_feed -->
        
        <div class="copyright">&copy; socialarc 2013</div>
        
    
    </footer>
</div><!--#footer_wrap-->    
    
    
</div><!--#wrapper-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>
<!-- local testing <script src="js/jquery-1.4.min.js"></script>-->
<script src="js/jquery.scrollTo.js"></script>
<script src="js/script.js"></script>

</body>
</html>
